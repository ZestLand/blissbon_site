<?php

require_once 'api.php';

AC\Autoloader::instance()->register_prefix( 'ACA\Pods', plugin_dir_path( ACA_PODS_FILE ) . 'classes/' );

$addon = new ACA\Pods\Pods( ACA_PODS_FILE );
$addon->register();