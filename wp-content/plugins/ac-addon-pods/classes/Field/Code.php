<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACP;

class Code extends Field {

	public function get_value( $id ) {
		return ac_helper()->html->codearea( parent::get_value( $id ) );
	}

	// Pro

	public function editing() {
		return new Editing\Textarea( $this->column() );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

}