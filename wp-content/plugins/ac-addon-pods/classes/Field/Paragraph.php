<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACP;
use AC;

class Paragraph extends Field {

	public function get_value( $id ) {
		return $this->column->get_formatted_value( parent::get_value( $id ) );
	}

	// Pro

	public function editing() {
		return new Editing\Textarea( $this->column() );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\WordLimit( $this->column() ),
		);
	}

}