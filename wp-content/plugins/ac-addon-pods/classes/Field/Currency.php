<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;
use ACP;

class Currency extends Field {

	// Pro

	public function editing() {
		return new Editing\Currency( $this->column() );
	}

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this->column() );

		return $model->set_data_type( 'numeric' );
	}

	public function filtering() {
		return new Filtering\Number( $this->column() );
	}

}