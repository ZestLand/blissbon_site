<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;
use ACP;

class Number extends Field {

	public function editing() {
		return new Editing\Number( $this->column() );
	}

	public function filtering() {
		return new Filtering\Number( $this->column() );
	}

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this->column() );

		return $model->set_data_type( 'numeric' );
	}

}