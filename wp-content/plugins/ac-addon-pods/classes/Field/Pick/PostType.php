<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;
use AC;
use AC\Collection;

class PostType extends Field\Pick {

	public function get_value( $id ) {
		return $this->column->get_formatted_value( new Collection( $this->get_raw_value( $id ) ) );
	}

	public function get_raw_value( $id ) {
		return $this->get_db_value( $id );
	}

	// Pro

	public function editing() {
		return new Editing\PickPosts( $this->column );
	}

	public function filtering() {
		return new Filtering\PickPosts( $this->column );
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\Post( $this->column ),
		);
	}

}