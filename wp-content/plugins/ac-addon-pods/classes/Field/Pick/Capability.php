<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;

class Capability extends Field\Pick {

	// Pro

	public function editing() {
		return new Editing\Pick( $this->column() );
	}

	public function filtering() {
		return new Filtering\Pick( $this->column() );
	}

	// Helpers

	public function get_options() {
		return parent::get_pick_field()->data_capabilities();
	}

}