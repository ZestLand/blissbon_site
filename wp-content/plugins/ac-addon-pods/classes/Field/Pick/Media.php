<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Export;
use ACA\Pods\Field;
use AC;
use AC\Collection;

class Media extends Field\Pick {

	// Display

	public function get_value( $id ) {
		return $this->column->get_formatted_value( new Collection( $this->get_raw_value( $id ) ) );
	}

	public function get_raw_value( $id ) {
		return (array) $this->get_ids_from_array( parent::get_raw_value( $id ) );
	}

	// Pro

	public function editing() {
		return new Editing\PickMedia( $this->column() );
	}

	public function export() {
		return new Export\File( $this->column() );
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\Image( $this->column() ),
		);
	}

}