<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;

class Role extends Field\Pick {

	// Pro

	public function editing() {
		return new Editing\Pick( $this->column() );
	}

	public function filtering() {
		return new Filtering\Pick( $this->column() );
	}

	// Pick

	public function get_options() {
		if ( ! class_exists( 'PodsField_Pick', true ) ) {
			return array();
		}

		$pod = new \PodsField_Pick();

		return $pod->data_roles();
	}

}