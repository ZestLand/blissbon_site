<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;
use AC;
use AC\Collection;

class User extends Field\Pick {

	// Display

	public function get_value( $id ) {
		return $this->column->get_formatted_value( new Collection( $this->get_raw_value( $id ) ) );
	}

	public function get_raw_value( $id ) {
		return (array) $this->get_ids_from_array( parent::get_raw_value( $id ) );
	}

	// Pro

	public function editing() {
		return new Editing\PickUsers( $this->column() );
	}

	public function filtering() {
		return new Filtering\PickUsers( $this->column() );
	}

	// Helper

	public function get_users( $user_ids ) {
		$names = array();

		if ( empty( $user_ids ) ) {
			return false;
		}

		foreach ( (array) $user_ids as $k => $user_id ) {
			$names[ $user_id ] = ac_helper()->user->get_display_name( $user_id );
		}

		return $names;
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\User( $this->column() ),
		);
	}

}