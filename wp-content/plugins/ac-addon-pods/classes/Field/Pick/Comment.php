<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;

class Comment extends Field\Pick {

	public function get_raw_value( $id ) {
		return $this->get_ids_from_array( parent::get_raw_value( $id ), 'comment_ID' );
	}

	// Pro

	public function editing() {
		return new Editing\PickComments( $this->column() );
	}

}