<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;

class MonthsOfYear extends Field\Pick {

	// Pro

	public function editing() {
		return new Editing\Pick( $this->column() );
	}

	public function filtering() {
		return new Filtering\Pick( $this->column() );
	}

	// Pick

	public function get_options() {
		return $this->get_pick_field()->data_months_of_year();
	}

}