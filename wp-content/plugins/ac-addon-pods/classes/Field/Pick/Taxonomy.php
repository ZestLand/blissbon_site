<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;

class Taxonomy extends Field\Pick {

	public function get_raw_value( $id ) {
		return $this->get_ids_from_array( parent::get_raw_value( $id ), 'term_id' );
	}

	// Pro

	public function editing() {
		return new Editing\PickTaxonomy( $this->column() );
	}

	public function filtering() {
		return new Filtering\PickTaxonomy( $this->column() );
	}

	// Taxonomy

	public function get_taxonomy() {
		return $this->column->get_field()->get( 'pick_val' );
	}

}