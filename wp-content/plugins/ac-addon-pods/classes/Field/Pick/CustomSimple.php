<?php

namespace ACA\Pods\Field\Pick;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Filtering;

class CustomSimple extends Field\Pick {

	public function editing() {
		return new Editing\PickCustom( $this->column() );
	}

	public function filtering() {
		return new Filtering\PickCustom( $this->column() );
	}

}