<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACP;

class Email extends Field {

	public function editing() {
		return new Editing\Email( $this->column() );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Meta( $this->column() );
	}

}