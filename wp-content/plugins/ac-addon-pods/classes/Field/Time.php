<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACP;

class Time extends Field {

	public function editing() {
		return new Editing( $this->column() );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

}