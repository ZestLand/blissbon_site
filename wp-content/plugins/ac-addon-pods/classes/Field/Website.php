<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACP;

class Website extends Field {

	public function get_value( $id ) {
		$field = $this->column()->get_pod_field();
		$target = $field['options']['website_new_window'] ? '_blank' : '_self';
		$url = parent::get_raw_value( $id );

		return ac_helper()->html->link( $url, str_replace( array( 'http://', 'https://' ), '', $url ), array( 'target' => $target ) );
	}

	// Pro

	public function editing() {
		return new Editing( $this->column() );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Meta( $this->column() );
	}

}