<?php

namespace ACA\Pods\Field;

use ACA\Pods\Editing;
use ACA\Pods\Field;
use ACA\Pods\Setting;
use ACP;

class Datetime extends Field {

	// Display

	public function get_value( $id ) {
		if ( ! parent::get_value( $id ) ) {
			return $this->column()->get_empty_char();
		}

		return $this->column->get_formatted_value( $this->get_raw_value( $id ) );
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this->column() );
	}

	public function editing() {
		return new Editing\Datetime( $this->column() );
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new Setting\Date( $this->column() ),
		);
	}

}