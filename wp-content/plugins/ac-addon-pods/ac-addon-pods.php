<?php
/*
Plugin Name: 		Admin Columns Pro - Pods
Version: 			1.2
Description: 		Supercharges your Admin Columns Pro with unique Pods columns.
Author: 			Admin Columns
Author URI: 		https://www.admincolumns.com
Plugin URI: 		https://www.admincolumns.com
Text Domain: 		codepress-admin-columns
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

define( 'ACA_PODS_FILE', __FILE__ );

require_once 'classes/Dependencies.php';

function aca_pods_loader_init() {
	$dependencies = new ACA_Pods_Dependencies( plugin_basename( ACA_PODS_FILE ) );
	$dependencies->check_acp( '4.3' );

	if ( ! function_exists( 'pods' ) ) {
		$dependencies->add_missing_plugin( __( 'Pods', 'pods' ), $dependencies->get_search_url( 'Pods' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	require_once 'bootstrap.php';
}

add_action( 'after_setup_theme', 'aca_pods_loader_init' );