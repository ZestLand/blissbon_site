<? get_header(); ?> 
<section>
	<div class="container">
		<div class="<? if(is_active_sidebar( 'sidebar' )): ?>col-sm-9<? else: ?>col-sm-12<? endif; ?>">
			<h1><? printf('Поиск по строке: %s', get_search_query()); ?></h1>
			<? if (have_posts()) : while (have_posts()) : the_post(); ?>
				<? get_template_part('loop'); ?>
			<? endwhile;
			else: echo '<p>Нет записей.</p>'; endif; ?>	 
			<? pagination(); ?>
		</div>
		<? get_sidebar(); ?>
	</div>
</section>
<? get_footer(); ?>