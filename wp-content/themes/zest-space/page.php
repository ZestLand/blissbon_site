<? 
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage zest-space
 */
get_header(); ?>

<? if ( have_posts() ) : the_post(); ?>
<div id="post-<? the_ID(); ?>" <? post_class(); ?>>
    <? the_content(); ?>
</div>

<? if(is_active_sidebar( 'sidebar' )){ ?>
<? get_sidebar(); ?>
<? } ?>
<? get_footer(); ?>