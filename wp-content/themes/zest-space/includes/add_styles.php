<?
add_action('wp_print_styles', 'add_styles');
function add_styles() {
    if(is_admin()) return false;
	
    wp_enqueue_style( 'main-styles', get_template_directory_uri().'/assets/css/style'.CSS_MIN.'.css?css_cache='.CSS_CACHE );
    
    
    //wp_enqueue_style( 'style-name', get_template_directory_uri().'/assets/css/style.css?css_cache='.CSS_CACHE );
}
