<?
add_action('wp_footer', 'add_scripts');
function add_scripts() {
    if(is_admin()) return false;
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery','//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js','','',true);
    
    wp_enqueue_script('main-scripts', get_template_directory_uri().'/assets/js/main'.JS_MIN.'.js?js_cache='.JS_CACHE,'','',true);
    
    //wp_enqueue_script('script-name', get_template_directory_uri().'/assets/js/main.js','','',true);
}