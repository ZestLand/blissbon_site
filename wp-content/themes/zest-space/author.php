<? get_header(); ?> 
<section>
	<div class="container">
		<div class="<? if(is_active_sidebar( 'sidebar' )): ?>col-sm-9<? else: ?>col-sm-12<? endif; ?>">
		    <? $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>
			<h1>Посты автора <? echo $curauth->nickname; ?></h1>
			<div class="media">
				<div class="media-left">
					<? echo get_avatar($curauth->ID, 64, '', $curauth->nickname, array('class' => 'media-object')); ?>
				</div>
			<div class="media-body">
				<h4 class="media-heading"><? echo $curauth->display_name; ?></h4>
				<? if ($curauth->user_url) echo '<a href="'.$curauth->user_url.'">'.$curauth->user_url.'</a>'; ?>
				<? if ($curauth->description) echo '<p>'.$curauth->description.'</p>'; ?>
			</div>
			</div>

			<? if (have_posts()) : while (have_posts()) : the_post(); ?>
				<? get_template_part('loop'); ?>
			<? endwhile;
			else: echo '<p>Нет записей.</p>'; endif; ?>	 
			<? pagination(); ?>
		</div>
		<? get_sidebar(); ?>
	</div>
</section>
<? get_footer(); ?>