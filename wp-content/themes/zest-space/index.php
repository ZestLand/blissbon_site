<?
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); ?> 
<section>
	<div class="container">
		<div class="<? if (is_active_sidebar( 'sidebar' )): ?>col-sm-9<? else: ?>col-sm-12<? endif; ?>">
			<h1><?
				if (is_day()) : printf('Daily Archives: %s', get_the_date());
				elseif (is_month()) : printf('Monthly Archives: %s', get_the_date('F Y'));
				elseif (is_year()) : printf('Yearly Archives: %s', get_the_date('Y'));
				else : 'Archives';
			endif; ?></h1>
			<? if (have_posts()) : while (have_posts()) : the_post(); ?>
				<? get_template_part('loop'); ?>
			<? endwhile;
			else: echo '<p>Нет записей.</p>'; endif; ?>	 
			<? pagination(); ?>
		</div>
		<? get_sidebar(); ?>
	</div>
</section>
<? get_footer(); ?>