<?php
/*
Template Name Posts: Video
*/
?>

<? get_header(); ?>
		<div class="<? if (is_active_sidebar( 'sidebar' )): ?>cl-1<? else: ?>cl-2<? endif; ?>">
			<? if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<article id="post-<? the_ID(); ?>" <? post_class(); ?>>
					<h1><? the_title(); ?></h1>
					<div class="meta">
						<p>Опубликовано: <? the_time('F j, Y в H:i'); ?></p>
						<p>Автор:  <? the_author_posts_link(); ?></p>
						<p>Категории: <? the_category(',') ?></p>
						<? the_tags('<p>Тэги: ', ',', '</p>'); ?>
					</div>
					<? the_content(); ?>
				</article>
			<? endwhile; ?>
			<? previous_post_link('%link', '<- Предыдущий пост: %title', TRUE); ?> 
			<? next_post_link('%link', 'Следующий пост: %title ->', TRUE); ?> 
			<? if (comments_open() || get_comments_number()) comments_template('', true); ?>
		</div>
		<? get_sidebar(); ?>
<? get_footer(); ?>
