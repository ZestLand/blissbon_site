<article id="post-<? the_ID(); ?>" <? post_class(); ?>>
	<h2><a href="<? the_permalink(); ?>"><? the_title(); ?></a></h2>
	<div class="meta">
		<p>Опубликовано: <? the_time('F j, Y в H:i'); ?></p>
		<p>Автор:  <? the_author_posts_link(); ?></p>
		<p>Категории: <? the_category(',') ?></p>
		<? the_tags('<p>Тэги: ', ',', '</p>'); ?>
	</div>
	<div class="row">
		<? if ( has_post_thumbnail() ) { ?>
			<div class="col-sm-3">
				<a href="<? the_permalink(); ?>" class="thumbnail">
					<? the_post_thumbnail(); ?>
				</a>
			</div>
		<? } ?>
		<div class="<? if ( has_post_thumbnail() ) { ?>col-sm-9<? } else { ?>col-sm-12<? }  ?>">
			<? the_content(''); ?>
		</div>
	</div>
</article>