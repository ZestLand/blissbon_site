<?

define('DEV', true);
if(DEV){
    define('CSS_CACHE', time());
    define('JS_CACHE', time());
} else{
    define('CSS_CACHE', 'a1');
    define('JS_CACHE', 'a1');
}

function typical_title() {
	global $page, $paged; 
	wp_title('', true, 'right');
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) echo " | $site_description";
	if ($paged >= 2 || $page >= 2) echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page));
}

register_nav_menus(array(
	'top' => 'Верхнее',
	'bottom' => 'Нижнее'
));

add_theme_support('post-thumbnails');
set_post_thumbnail_size(300, 300);
//add_image_size('big-thumb', 400, 400, true);

register_sidebar(array(
	'name' => 'Сайдбар',
	'id' => "sidebar",
	'description' => 'Обычная колонка в сайдбаре',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => "</div>\n",
	'before_title' => '<span class="widgettitle">',
	'after_title' => "</span>\n",
));

function pagination() {
	global $wp_query;
	$big = 999999999;
	$links = paginate_links(array(
		'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'type' => 'array',
		'prev_text'    => 'Назад',
    	'next_text'    => 'Вперед',
		'total' => $wp_query->max_num_pages,
		'show_all'     => false,
		'end_size'     => 15,
		'mid_size'     => 15,
		'add_args'     => false,
		'add_fragment' => '',
		'before_page_number' => '',
		'after_page_number' => ''
	));
 	if( is_array( $links ) ) {
	    echo '<ul class="pagination">';
	    foreach ( $links as $link ) {
	    	if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>$link</li>";
	        else echo "<li>$link</li>"; 
	    }
	   	echo '</ul>';
	 }
}

// replace contact form 7 submit input with button
wpcf7_add_form_tag( 'submit', 'zest_wpcf7_submit_form_tag' );
function zest_wpcf7_submit_form_tag($tag){
    $class = wpcf7_form_controls_class( $tag->type );

	$atts = array();

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'signed_int', true );

	$value = isset( $tag->values[0] ) ? $tag->values[0] : '';

	if ( empty( $value ) ) {
		$value = __( 'Send', 'contact-form-7' );
	}

	$atts['type'] = 'submit';
	$atts['value'] = $value;

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf( '<button %1$s />'.$value.'</button>', $atts );
	return $html;
}

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
    //$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    $host = 'http://'. $_SERVER['HTTP_HOST'];
    $html = str_replace($host, '', $html);
   return $html;
}
include 'includes/shortcodes';
include 'includes/zest_parameters.php';
include 'includes/classes.php';
include 'includes/add_scripts.php';
include 'includes/add_styles.php';