<?
class bootstrap_menu extends Walker_Nav_Menu {
	private $open_submenu_on_hover;

	function __construct($open_submenu_on_hover = true) {
        $this->open_submenu_on_hover = $open_submenu_on_hover;
    }

	function start_lvl(&$output, $depth = 0, $args = array()) {
		$output .= "\n<ul class=\"dropdown-menu\">\n";
	}
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		$item_html = '';
		parent::start_el($item_html, $item, $depth, $args);
		if ( $item->is_dropdown && $depth === 0 ) {
		   if (!$this->open_submenu_on_hover) $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"', $item_html);
		   $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
		}
		$output .= $item_html;
	}
	function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
		if ( $element->current ) $element->classes[] = 'active';
		$element->is_dropdown = !empty( $children_elements[$element->ID] );
		if ( $element->is_dropdown ) {
		    if ( $depth === 0 ) {
		        $element->classes[] = 'dropdown';
		        if ($this->open_submenu_on_hover) $element->classes[] = 'show-on-hover';
		    } elseif ( $depth === 1 ) {
		        $element->classes[] = 'dropdown-submenu';
		    }
		}
		parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
	}
}